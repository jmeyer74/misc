#!/bin/zsh

urls=(
301http://crusoecloud.com
301http://www.crusoecloud.com
200https://crusoecloud.com
200https://www.crusoecloud.com
200https://blog.crusoecloud.com

301http://console.crusoecloud.com
200https://console.crusoecloud.com

301http://api.crusoecloud.com
200https://api.crusoecloud.com

301http://docs.crusoecloud.com
200https://docs.crusoecloud.com

301http://crusoedatacenters.com
301https://crusoedatacenters.com
301http://www.crusoedatacenters.com
301https://www.crusoedatacenters.com

301http://crusoedatacenterservices.com
301https://crusoedatacenterservices.com
301http://www.crusoedatacenterservices.com
301https://www.crusoedatacenterservices.com
)

RED='\033[0;31m'
NC='\033[0m' # No Color

while true; do
  for urlp in $urls; do
    code=$(echo "$urlp" | head -c 3)
    url=$(echo "$urlp" | tail -c +4)

    res=$(curl -m 3 -s -o /dev/null -w "%{http_code}" $url)
    if [[ $res != $code ]]; then
      echo -n "${RED}"
    fi
    echo "$res : $code : $url${NC}"
  done
  echo "------------------------"
  sleep 1
done
