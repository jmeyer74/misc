#!/bin/zsh

tested=0
bad=0

echo "getting IPs of VMs from cloud-admin"
for i in $(cloud-admin vms list --format json | jq -r '.[].ip' | grep "^\d"); do
    echo -n "testing $i"
    timeout -s2 2 /usr/bin/nc -z $i 22 >/dev/null 2>&1

    if [ $? != 0 ]; then
      echo " is unreachable"
      ((bad+=1))
    else
      echo -n "                          \r"
    fi

    ((tested+=1))
done

echo "                                   \r"
echo "Tested $tested"
echo "Bad: $bad"