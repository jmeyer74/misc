#!/usr/bin/env perl

# all.txt should be a TSV paste of columns F,G,H from this doc: https://docs.google.com/spreadsheets/d/1wVLNI5D-T2y1V-L-2LdtZsEjgTWY1tvVe0Of6kQwOLM/edit#gid=2133748158
#
$file = "all.txt";
open(FILE, "$file");
my @f = <FILE>;
close(FILE);


foreach(@f){
    $_ =~ s/\n//;
    next if ($_ =~ /^\#/);

    my ($pw, $mac, $ip) = split("\t");
    next if (!$pw);

    my $exit_code=system("ping -c 1 -W 1 $ip > /dev/null 2>&1");
    next if $exit_code != 0;

    # check power state, dont bother on off machines
    my $pcmd = "/home/jmeyer/sum_2.7.0_Linux_x86_64/sum -i $ip -u ADMIN -p $pw -c GetPowerStatus";
    $pout = `$pcmd`;
    if($pout !~ /Power status..............On/){
        #print "$ip is off or password is bad, skippity\n";
        next;
    }

    print "$ip\t";
    my $cmd = "/home/jmeyer/sum_2.7.0_Linux_x86_64/sum -i $ip -u ADMIN -p $pw -c CheckSensorData";
	$out = `$cmd`;

    my @lines = split('\n', $out);
    foreach my $line (@lines){
        if( $line =~ /(CPU|GPU)(\d) Temp\s*\|\s*(\d*)C\/.*/){
            print "$1$2\t$3\t";
        }
        else{
            #print "no match $line\n";
        }
    }
    print "\n";
}
