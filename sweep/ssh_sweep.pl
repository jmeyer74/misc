#!/usr/bin/env perl

$file = "servers.txt";
open(FILE, "$file");
my @f = <FILE>;
close(FILE);



foreach(@f){
    $_ =~ s/\n//;
    next if ($_ =~ /^\#/);
    
    my ($ip) = split("\t");
    next if (!$ip);

    my $cmd = "ssh -o StrictHostKeyChecking=no $ip sudo lvs -a ";
    print "$cmd $ip \t";
	print `$cmd`;
}
close(OUT);
