#!/usr/bin/env perl

$file = "hosts.txt";
open(FILE, "$file");
my @f = <FILE>;
close(FILE);

$lic = "9-lic.txt";
open(FILE, "$lic");
my @l = <FILE>;
close(FILE);
my %lic;
foreach(@l){
	my ($lmac, $ljson) = $_ =~ /([\w\d]{12});\s(.*)/;
	#print "$lmac, $ljson\n\n";
	$lic{$lmac} = $ljson;
}

foreach(@f){
    $_ =~ s/\n//;
    next if ($_ =~ /^\#/);
    my ($pw, $mac, $ip) = split("\t");
    	next if (!$pw || !$mac || !$ip);		

	
	my $cmd = "./sum_2.8.1_Linux_x86_64/sum -i $ip -u ADMIN -p $pw -c ActivateProductKey --key '$lic{$mac}' ";
	#my $cmd = "./SMCIPMITool_2.26.0_build.220209_bundleJRE_Linux_x64/SMCIPMITool $ip ADMIN $pw ipmi lan mac ";
	print "$cmd \n";
	print `$cmd`;
}


