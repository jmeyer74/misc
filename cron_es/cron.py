#this scrip is a stopgap for CLOUD-235
#jmeyer@crusoeenergy.com
#it is meant to make trivial queries to ES and sending an opsgenie alarm

#expects ENV(GENIEKEY) and the requests python library to be installed, done each time in cron
# INSTALLATION NOTES:
# kc create configmap -n es --dry-run=client es-cron-script --from-file=cron.py --output yaml | tee script_cm.yaml
# kc create secret generic es-cron-ogkey -n es --dry-run=client --from-literal=GENIEKEY=#### --output yaml | tee script_cm.yaml

import json
import requests
import os

host = "http://elasticsearch-master.es.svc.cluster.local:9200"

og_api_key = os.environ['GENIEKEY']

query = """
{
   "query" : {

     "bool":{
       "filter":[
       {
           "multi_match": {
             "type": "phrase",
             "query": "vfio_bar_restore",
             "lenient": true
           }
         },
         {
           "range": {
             "@timestamp": {
               "gte": "now-1h/d",
               "lt": "now/d"
             }
           }
         }
       ]
     }
   }
}
"""
json_query = json.loads(query)
req_headers = {'content-type': 'application/json'}

if os.environ.get('ELASTICSEARCH_PASSWORD') != None :
    creds=(os.environ['ELASTICSEARCH_USERNAME'], os.environ['ELASTICSEARCH_PASSWORD'])
else:
    creds=None

try:
    r = requests.get(host + "/hardware*/_count",
                  json=json_query,
                  auth=creds,
                 verify=False,
                 headers=req_headers)
    if r.status_code != 200:
        print("Fail http code: " + str(r.status_code))
        exit(1)
    response = r.json()
except requests.exceptions.RequestException as e:
    raise SystemExit(e)

if response["count"] > 0:
    print("sending alert!")

    og_req_headers = {
        'Authorization': "GenieKey "+ og_api_key,
        'content-type': 'application/json'
    }
    og_payload = {
        "message": "vfio_bar_restore log error",
        "description": str(response["count"]) +" vfio_bar_restore errors are occurring in elasticsearch",
        "priority":"P2"
    }

    print(og_payload)
    og_r = requests.post("https://api.opsgenie.com/v2/alerts",
                       json=og_payload,
                     headers=og_req_headers)

    print(og_r.status_code)
    print(og_r.json())
else:
    print("check complete, no issue")