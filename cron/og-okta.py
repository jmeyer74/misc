#!/usr/bin/env python3
# currently running cron-minute-ly in island-prod in my account on the jmeyer-misc VM 
# jmeyer@crusoeenergy.com


import json
import yaml
import requests
import os
import sys
import pickle

og_api_key      = os.environ['OG_API']
og_schedule_id  = os.environ['OG_SCHEDULE_ID']
okta_key        = os.environ['OKTA_API']
okta_oc_group   = os.environ['OKTA_GROUP_ID']
okta_state_file = os.path.realpath(__file__) + "_"+og_schedule_id+"_state.pickle"

okta_baseurl =  "https://crusoeenergy.okta.com"

output = True
errors = True

og_headers = {
    "Authorization": f"GenieKey {og_api_key}",
    "Content-Type": "application/json"
}

okta_headers = {
    "Authorization": f"SSWS {okta_key}",
    "Content-Type": "application/json"
}


url = f"https://api.opsgenie.com/v2/schedules/{og_schedule_id}/on-calls"
response = requests.get(url, headers=og_headers)

if response.status_code != 200:
    if errors: print(f"Something went wrong getting opsgenie. Status code: {response.status_code}")
    sys.exit(1)
data = response.json()
og_oncall_email = data['data']['onCallParticipants'][0]['name']
if output: print(f"The OG on-call person is: {og_oncall_email}")

grp_users = False
if os.path.exists(okta_state_file):
    if output: print("cache present, loading, ", end='')
    with open(okta_state_file, 'rb') as f:
        grp_users = pickle.load(f)
    if output:
        print("cached users:")
        print(json.dumps(grp_users, indent=2))
else:
    grp_url = f"{okta_baseurl}/api/v1/groups/{okta_oc_group}/users/"
    grp_res = requests.get(grp_url, headers=okta_headers)
    if grp_res.status_code != 200:
        if errors: print(f"Something went wrong w okta groups. Status code: {g_res.status_code}")
        sys.exit(1)
    grp_users = grp_res.json()
    #pickle this data
    with open(okta_state_file, 'wb') as f:
        pickle.dump(grp_users,f)



found = False
for user in grp_users:
    if user['profile']['email'] == og_oncall_email:
        found = True
        break


if not found:
    if output: print("USER CHANGED!")

    #re-getch grp users as mismatche are not handled
    grp_url = f"{okta_baseurl}/api/v1/groups/{okta_oc_group}/users/"
    grp_res = requests.get(grp_url, headers=okta_headers)
    if grp_res.status_code != 200:
        if errors: print(f"Something went wrong w okta groups. Status code: {g_res.status_code}")
        sys.exit(1)
    grp_users = grp_res.json()

    url = f"{okta_baseurl}/api/v1/users?limit=200"
    response = requests.get(url, headers=okta_headers)
    if response.status_code != 200:
        if errors: print(f"Something went wrong with okta. Status code: {response.status_code}")
        sys.exit(1)
    r = response.json()
    data = yaml.load(str(r),yaml.SafeLoader)

    users = {}
    for u in data:
        users[u['profile']['email']] = u['id']

    #remove all okta users
    for user in grp_users:
        uid_del = users[user['profile']['email']]
        if output: print(f"removing {user['profile']['email']} {uid_del}")
        del_url = f"{okta_baseurl}/api/v1/groups/{okta_oc_group}/users/{uid_del}"
        d_response = requests.delete(del_url, headers=okta_headers)
        if d_response.status_code != 204: print(f"delete error {del_url} {d_response.status_code}")
    #add new user
    uid_new = users[og_oncall_email]
    if output: print(f"adding {og_oncall_email} {uid_new}")
    new_url = f"{okta_baseurl}/api/v1/groups/{okta_oc_group}/users/{uid_new}"
    n_response = requests.put(new_url, headers=okta_headers)
    if n_response.status_code != 204: print(f"new user error {new_url} {n_response.status_code}")

    os.remove(okta_state_file)




