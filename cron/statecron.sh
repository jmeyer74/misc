#!/bin/bash
DATE=$(/bin/date +%Y%m%d-%H:%M)
export GOOGLE_APPLICATION_CREDENTIALS=~/.gcp/island-prod-876d562e0c5c.json

/Users/jmeyer/go/bin/cloud-admin vms list --format=json > /Users/jmeyer/json/vms_$DATE.json 
/Users/jmeyer/go/bin/cloud-admin nodes list --format=json > /Users/jmeyer/json/nodes_$DATE.json 

/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/bin/gsutil cp /Users/jmeyer/json/vms_$DATE.json gs://cloud-admin-json
/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/bin/gsutil cp /Users/jmeyer/json/nodes_$DATE.json gs://cloud-admin-json

#rm /Users/jmeyer/json/vms_$DATE.json
#rm /Users/jmeyer/json/nodes_$DATE.json