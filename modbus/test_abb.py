#!/usr/bin/env python3
#pip3 install pyModbusTCP

from pyModbusTCP.client import ModbusClient
c = ModbusClient(host="172.18.136.145", port=502, unit_id=1, auto_open=True)


addy = 7001
regs = c.read_holding_registers(addy, 2)

if regs:
    print(addy)
    print(regs)
else:
    print("read error")