#!/usr/bin/env python3

#https://crusoeenergy.slack.com/archives/C02KNTVCMTR/p1671808153990669
#reserved up to 103584

start     = 88585
#end = start + 5
end       = start + 5000 - 1
type_byte = 0x10
total_bits = 128

import pyqrcode
import png
from pyqrcode import QRCode

type_bits = type_byte << total_bits - 8
#print("{0:b}".format(type_bits))

j = 0
for i in range (start, end+1):
    j=j+1
    id=i



    #EPC
    epc=id ^ type_bits
    #print("bin: {0:b}".format(epc), end='') #bin
    print("\"{0:x}\"".format(epc), end='') #hex
    #print("{0:d}".format(epc), end='') #dec

    #seq number
    print(", "+str(j).rjust(5, "0")+" " ,end='')

    #hoo man message
    print(", "+str(id), end='')

    #barcode message https://gitlab.com/crusoeenergy/crusoe-plugin/-/blob/master/src/components/inventory/labels/PrintableLabels.tsx#L33
    qrstring = "\"https://asset.crusoe.ai/"+str(id)+"\""
    print(", " +qrstring, end='')
    #print("\t"+qrstring, end='')
    #url = pyqrcode.create(qrstring)
    #url.svg("out/"+str(id)+".svg", scale = 8)

    print("\tQRCode")
