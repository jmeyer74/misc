#!/usr/bin/env perl

$file = "hosts.txt";
open(FILE, "$file");
my @f = <FILE>;
close(FILE);

foreach(@f){
    $_ =~ s/\n//;
    next if ($_ =~ /^\#/);
    next if !$_;
    my ($mac, $ip, $hn) = split("\t");

	print <<EOF
host $hn {
  hardware ethernet $mac;
  fixed-address $ip;
}

EOF
}


print "\n\n";


foreach(@f){
    $_ =~ s/\n//;
    next if ($_ =~ /^\#/);
    next if !$_;
    my ($mac, $ip, $hn) = split("\t");

    my $last_oct = ($ip =~ /\.(\d+)$/)[0];

	print <<EOF
$hn ansible_host=$ip client_host=10.64.1.$last_oct
EOF
}