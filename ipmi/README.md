# useful commands in ipmi

## SCM IPMI tool

confirm GPU installation type

`/home/jmeyer/SMCIPMITool/SMCIPMITool_2.25.0_build.210326_bundleJRE_Linux_x64/SMCIPMITool 172.17.5.51 ADMIN XXXXXX hwinfo`
`/home/jmeyer/SMCIPMITool/SMCIPMITool_2.25.0_build.210326_bundleJRE_Linux_x64/SMCIPMITool 172.17.5.51 ADMIN XXXXXX hwinfo | grep GPU1 | awk '{ print $6 }'`
### SUM
get bios config
`/home/jmeyer/sum_2.7.0_Linux_x86_64/sum -i 172.17.5.51 -u ADMIN -p XXXXXX -c getcurrentbioscfg --file some.xml`

clone from file and write out to a new machines

note machiens dont seem to reboot themselves
`/home/jmeyer/sum_2.7.0_Linux_x86_64/sum -i 172.17.5.54 -u ADMIN -p XXXXXX -c changebioscfg --file out.xt --skip_unknown --reboot --post_complete`


get the mac of the pxe

`/home/jmeyer/sum_2.7.0_Linux_x86_64/sum -i 172.17.5.54 -u ADMIN -p XXXXXX -c getcurrentbioscfg | grep -oP "XXV710 for 25GbE SFP28 - \K(............)(?=</Option>)" | uniq -w 11`