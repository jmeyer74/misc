#!/usr/bin/env python3
# status code checker
import requests
import csv
import time

SLEEP = 0 # Time in seconds the script should wait between requests
url_list = []
url_statuscodes = []
url_statuscodes.append(["url","status_code"]) # set the file header for output
def getStatuscode(url):
    try:
        r = requests.head(url,verify=True,timeout=1) # it is faster to only request the header
        return ([r.ok, r.status_code, r.next.url])
    except:
        return ([r.ok, r.status_code])
# Url checks from file Input
# use one url per line that should be checked
with open('urls.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        url_list.append(row[0])

# Loop over full list
for url in url_list:
    check = [url,getStatuscode(url)]
    time.sleep(SLEEP)
    url_statuscodes.append(check)
    print(check[0].ljust(50), end='')
    print(check[1:])

# Save file
# with open("urls_withStatusCode.csv", "w", newline="") as f:
#     writer = csv.writer(f)
#     writer.writerows(url_statuscodes)