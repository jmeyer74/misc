#!/usr/bin/perl
# hosted on personal dropbox: https://www.dropbox.com/s/isoyb0u8hb5rdhd/nginx_logs.txt?dl=0
# find all the totals for ips in the given range
# open logs to find the traffic. 3832

open(FILE, "nginx_logs.txt");

my @DATA = <FILE>;
close(FILE);

@masks = (
"192.0.0.0/8",
"93.180.0.0/16",
"80.91.0.0/16",
"192.235.0.0/16",
"217.168.17.2/29"
);

%totals;

foreach(@DATA){
    my ($a,$b,$c,$d, $mess) = m/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})(\s.*)/;

    my $ip = 0;
    $ip += (int($a) << 24);
    $ip += (int($b) << 16);
    $ip += (int($c) << 8);
    $ip += int($d);

    foreach $msk (@masks){
        my $mask = 0;
        my ($ma, $mb, $mc, $md, $bits) = ($msk =~ m/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\/(\d+)/);

        $mask += (int($ma) << 24);
        $mask += (int($mb) << 16);
        $mask += (int($mc) << 8);
        $mask +=  int($md);

        my $ok = 0;
        my $test = 0xFFFFFFFF;
        for($i = 32 - $bits; $i > 0; $i--){
            $test = $test << 1;
        }

        #printf "%b %b %b %b %b \n", $mask, $ip, $test, ($mask & $test), ($ip & $test);
        if(($mask & $test) == ($ip & $test)){
            #print "match\n";
            $ok = 1;
        }


        if($ok){
            $totals{$msk} = int($totals{$msk}) + 1;
         }
    }
}

foreach(keys(%totals)){
    print "$_ : $totals{$_}\n";
}
