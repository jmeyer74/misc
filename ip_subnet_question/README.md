# Devops IP Subnet question

## Objective

Give the candidate a log file.  Ask them to find all the rows where the data is on multiple subnets. 
This is meant to be a tougher problem than a grep or substring match.

Link to off-repo [tgz file](https://github.com/turbohoje/files/raw/main/nginx_logs.txt.gz)

## Key Skills

1. Efficient use of code to solve 1-off on the fly problems
1. Understanding of network subnets and IP addresses
1. Parsing of delimited log files

There are libraries that do all of this.

```
>>> import ipaddress
>>> ipaddress.ip_address('192.168.0.1') in ipaddress.ip_network('192.168.0.0/24')
True
```

If they pick something like this out of the gate, ask a follow up question to make sure they understand how the octets and masks work.

e.g. "why can you do all this with grep or substring?"

## Subnets

These values match some of the data in the file, and not all of them can be substring'd.  
e.g. /^192.*/ /^80.91.*/ but /^217.168.17.[1-7].*/ isnt really an answer as it is not doing the CIDR math.

[data file download](https://www.dropbox.com/s/isoyb0u8hb5rdhd/nginx_logs.txt?dl=0)
```
"192.0.0.0/8",
"93.180.0.0/16",
"80.91.0.0/16",
"192.235.0.0/16",
"217.168.17.2/29"

```

